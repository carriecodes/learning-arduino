//by /u/Zy14rk on /r/arduino

const byte potPin = 0;
const byte redPin = 11;
const byte greenPin = 10;
const byte bluePin = 9;
 
byte color[3]{0, 0, 0};
 
typedef struct timeStruct{
  uint32_t oldTime;
  uint32_t newTime;
  uint32_t intervalMS;
} timer;
 
typedef struct potStruct{
  int value;
  byte range;  
} pot;
 
timer timeGet;
timer *timeSet = &timeGet;
pot potGet;
pot *potSet = &potGet;
 
void intializeStructs(timer *t){
  t->oldTime = millis();
  t->intervalMS = 10;
}
 
bool checkTime(timer *t){
  t->newTime = millis();
  if (timeGet.newTime > (timeGet.oldTime + timeGet.intervalMS)){
    t->oldTime = millis();
    return true;
  } else {
    return false;
  }
}
 
void getPot(pot *p){
  p->value = analogRead(potPin);
  p->range = map(potGet.value, 0, 512, 0, 1);
  Serial.print("pot: ");
  Serial.print(potGet.value);
  Serial.print(" || range: ");
  Serial.println(potGet.range);  
}
 
void getColor(byte col){
  switch (col){
    case 0:
      color[0] = map(potGet.value, 0, 511, 255, 0);
      color[1] = map(potGet.value, 0, 511, 0, 255);
      color[2] = 0;
      break;
    case 1:
      color[0] = 0;
      color[1] = map(potGet.value, 512, 1024, 255, 0);
      color[2] = map(potGet.value, 512, 1024, 0, 255);
      break;
  }
}
 
void setColor() {
  analogWrite(redPin, color[0]);
  analogWrite(greenPin, color[1]);
  analogWrite(bluePin, color[2]);
}
 
void setup(){
  Serial.begin(9600);
  pinMode(redPin, OUTPUT);
  pinMode(greenPin, OUTPUT);
  pinMode(bluePin, OUTPUT);
  intializeStructs(timeSet);
}
 
void loop(){
  if (checkTime(timeSet)){
    getPot(potSet);
    getColor(potGet.range);
    setColor();
  }
}
