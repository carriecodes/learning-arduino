/*
 * this is really crappy code
 * 
 * 
 */

int redPin = 11;
int greenPin = 10;
int bluePin = 9;

int redVal = 255;
int greenVal = 0;
int blueVal = 0;

int SPEED = 10;

#define COMMON_ANODE

void setup() {
  // put your setup code here, to run once:
  pinMode(redPin, OUTPUT);
  pinMode(greenPin, OUTPUT);
  pinMode(bluePin, OUTPUT);
}

void loop() {
  // put your main code here, to run repeatedly:
  setColor();
  makeYellow();
  makeGreen();
  makeAqua();
  makeBlue();
  makePink();
  makeRed();
}

void makeYellow() {
  for (int i = 0; i < 255; i++) {
    greenVal++;
    setColor();
  }
}

void makeGreen() {
  for (int i = 0; i < 255; i++) {
    redVal--;
    setColor();
  }
}

void makeAqua() {
  for (int i = 0; i < 255; i++) {
    blueVal++;
    setColor();
  }
}

void makeBlue() {
  for (int i = 0; i < 255; i++) {
    greenVal--;
    setColor();
  }
}

void makePink() {
  for (int i = 0; i < 255; i++) {
    redVal++;
    setColor();
  }
}

void makeRed() {
  for(int i = 0; i < 255; i++) {
    blueVal--;
    setColor();
  }
}

 void setColor() {
  int red = redVal;
  int green = greenVal;
  int blue = blueVal;
  
  #ifdef COMMON_ANODE
    red = 255 - redVal;
    green = 255 - greenVal;
    blue = 255 - blueVal;
  #endif
    analogWrite(redPin, red);
    analogWrite(greenPin, green);
    analogWrite(bluePin, blue);
    delay(SPEED);
 }

